package com.api.cache.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.api.cache.dao.IPetDao;
import com.api.cache.dto.TPet;
import com.api.cache.entity.Pet;
import com.api.cache.util.Constants;
import com.api.cache.util.Constants.OBSERV;

@Service
public class PetServiceImpl implements IPetService {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	IPetDao petDao;

	@Cacheable(cacheNames = Constants.CACHE_LIST_PET, unless = "#result == null")
	@Override
	public List<Pet> findAll() {

		log.info(">>> method Service findAll()");
		List<Pet> lstPet = new ArrayList<Pet>();
		List<TPet> lst = petDao.findByStatus(Constants.STATUS.ACTIVO.getCodigo());
		lst.stream().forEach(p -> lstPet.add(setPet(p)));

		return lstPet;
	}

	@Cacheable(cacheNames = Constants.CACHE_PET, key = "#id" , unless = "#result == null")
	@Override
	public Pet findByID(Long id) {
		log.info(">>> method Service findById()");
		Optional<TPet> p = petDao.findById(id);
		if (p.isPresent()) {
			return setPet(p.get());
		} else {
			return null;
		}
	}

	@CacheEvict(cacheNames = Constants.CACHE_LIST_PET, allEntries = true)
	@Override
	public Pet save(Pet pet) {

		log.info(">>> method Service save()");
		TPet p = new TPet();
		p.setAddress(pet.getAddress());
		p.setBirthday(pet.getBirthday());
		p.setName(pet.getName());
		p.setStatus(Constants.STATUS.ACTIVO.getCodigo());

		p.setAuditObs(OBSERV.CREATE);
		p.setAuditFecope(null);
		p.setAuditFecre(LocalDateTime.now());
		p.setAuditIP("127.0.0.0");
		p.setAuditUser("admin");
		p = petDao.save(p);
		return setPet(p);
	}

	@CacheEvict(cacheNames = Constants.CACHE_LIST_PET, allEntries = true)
	@CachePut(cacheNames = Constants.CACHE_PET, key = "#pet.id", unless = "#result == null")
	@Override
	public Pet update(Pet pet) {
		log.info(">>> method Service update()");
		Optional<TPet> pO = petDao.findById(pet.getId());

		TPet p = new TPet();
		p.setAddress(pet.getAddress());
		p.setBirthday(pet.getBirthday());
		p.setName(pet.getName());
		p.setPetId(pet.getId());
		p.setStatus(Constants.STATUS.ACTIVO.getCodigo());

		p.setAuditObs(OBSERV.UPDATE);
		p.setAuditFecope(LocalDateTime.now());
		p.setAuditIP("127.0.0.0");
		p.setAuditUser("admin");
		p.setAuditFecre(pO.get().getAuditFecre());

		p = petDao.save(p);
		return setPet(p);
	}

	private Pet setPet(TPet p) {
		Pet pet = new Pet();
		pet.setStatus(p.getStatus());
		pet.setAddress(p.getAddress());
		pet.setBirthday(p.getBirthday());
		pet.setId(p.getPetId());
		pet.setName(p.getName());
		return pet;
	}
}
