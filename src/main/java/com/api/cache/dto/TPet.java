package com.api.cache.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.api.cache.util.Constants.OBSERV;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "t_pet")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TPet implements Serializable {
	
	private static final long serialVersionUID = -3628251033357209748L;

	@Id
	@SequenceGenerator(name = "sequence_pet", sequenceName = "sq_pets", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_pet")
	@Column(name = "pet_iident")
	private Long petId;

	@Column(name = "pet_name")
	private String name;

	@Column(name = "pet_address")
	private String address;

	@Column(name = "pet_birthdate")
	private LocalDateTime birthday;

	@Column(name = "pet_cstatu")
	private String status;

	@Column(name = "audit_dfecope")
	private LocalDateTime auditFecope;

	@Column(name = "audit_dfecre")
	private LocalDateTime auditFecre;

	@Column(name = "audit_vipad")
	private String auditIP;

	@Column(name = "audit_vobs")
	@Enumerated(EnumType.STRING)
	private OBSERV auditObs;

	@Column(name = "audit_vuser")
	private String auditUser;

}
