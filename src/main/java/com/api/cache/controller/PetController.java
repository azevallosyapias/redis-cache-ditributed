package com.api.cache.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.api.cache.entity.Pet;
import com.api.cache.service.IPetService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/pet")
@Api(tags = "Managment Pets")
public class PetController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	IPetService service;

	@ApiOperation(value = "Get all pets", notes = "Get all pets")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<Pet> findAllPets() {
		log.info("Gettmapping: / list all pets");
		return service.findAll();
	}

	@ApiOperation(value = " ", notes = "Get one Pet")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid pet id"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Pet not found") })
	@GetMapping(path = "/getByID/{idPet}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public Pet findById(@PathVariable(value = "idPet") Long idPet) {
		log.info("Getmapping: find by id");
		return service.findByID(idPet);
	}
	
	@ApiOperation(value = "Create Pet", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "CREATED"),
			@ApiResponse(code = HttpServletResponse.SC_METHOD_NOT_ALLOWED, message = "Invalid input") })
	@PostMapping(path = "/createPet", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Pet savePet(@RequestBody Pet pet) {
		log.info("PostMapping save Pet");
		return service.save(pet);
	}

	@ApiOperation(value = "", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid pet id"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Pet not found") })
	@PutMapping(path = "/updatePet/{petId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public Pet updatePet(@RequestBody Pet pet, @PathVariable("petId") Long petId) {
		log.info("PutMapping update Pet: " + pet.toString());
		pet.setId(petId);
		return service.update(pet);
	}

}
