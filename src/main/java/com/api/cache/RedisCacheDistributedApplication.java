package com.api.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class RedisCacheDistributedApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisCacheDistributedApplication.class, args);
	}

}
