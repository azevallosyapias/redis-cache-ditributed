package com.api.cache.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/cache/")
public class CacheController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	CacheManager cacheManager;

	@ApiOperation(value = "Clear cache", notes = "")
	@PostMapping(path = "clear/{nameCache}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void clearCache(@PathVariable String nameCache) {
		log.info("Clearing cache: " + nameCache);
		cacheManager.getCache(nameCache).clear();
	}

}
