package com.api.cache.service;

import java.util.List;

import com.api.cache.entity.Pet;

public interface IPetService {

	public List<Pet> findAll();

	public Pet findByID(Long id);

	public Pet save(Pet pet);

	public Pet update(Pet pet);

}
