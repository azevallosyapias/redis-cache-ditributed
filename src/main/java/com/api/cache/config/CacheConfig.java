package com.api.cache.config;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import com.api.cache.util.Constants;


@Configuration
public class CacheConfig extends CachingConfigurerSupport {

	@Bean
	public CacheManager cacheManager(RedisConnectionFactory redisCF) {
		Map<String, RedisCacheConfiguration> redisCCMap = new HashMap<String, RedisCacheConfiguration>();
		redisCCMap.put(Constants.CACHE_LIST_PET, addConfig(1, ChronoUnit.DAYS)); // clear cache daily
		redisCCMap.put(Constants.CACHE_PET, addConfig(1, ChronoUnit.DAYS));

		return RedisCacheManager.builder(redisCF).withInitialCacheConfigurations(redisCCMap).build();
	}

	private static RedisCacheConfiguration addConfig(int time, ChronoUnit metric) {
		return RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.of(time, metric));
	}

}
