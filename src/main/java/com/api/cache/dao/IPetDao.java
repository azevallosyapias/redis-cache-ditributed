package com.api.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.api.cache.dto.TPet;

public interface IPetDao extends CrudRepository<TPet, Long> {

	public List<TPet> findByStatus(String status);

}
