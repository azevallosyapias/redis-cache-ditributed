package com.api.cache.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Pet implements Serializable{

	private static final long serialVersionUID = -2790660240052198951L;
	
	private Long id;
	private String name;
	private String address;
	private LocalDateTime birthday;
	private String status;
	
}
