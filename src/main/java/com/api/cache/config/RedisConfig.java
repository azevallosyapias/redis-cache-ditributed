package com.api.cache.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

import lombok.extern.log4j.Log4j2;

@Configuration
@Log4j2
public class RedisConfig {

	@Value("${redis.server}")
	private String redisServer;

	@Value("${redis.port}")
	private int redisPort;

	@Bean
	public LettuceConnectionFactory redisCF() {
		log.info("Redis configuration: "+redisServer+" : "+redisPort);
		return new LettuceConnectionFactory(new RedisStandaloneConfiguration(redisServer, redisPort));
	}

}
