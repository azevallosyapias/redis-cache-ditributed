package com.api.cache.util;

import lombok.Getter;

public class Constants {

	public static final String CACHE_LIST_PET ="pets";
	
	public static final String CACHE_PET ="pet";

	public static enum STATUS {
		ACTIVO("A", "Activo"), ELIMINADO("X", "Deshabilitado");

		@Getter
		private String codigo;
		@Getter
		private String message;

		private STATUS(String codigo, String message) {
			this.codigo = codigo;
			this.message = message;
		}

	}

	public enum OBSERV {
		CREATE, UPDATE, DELETE;
	}

}
